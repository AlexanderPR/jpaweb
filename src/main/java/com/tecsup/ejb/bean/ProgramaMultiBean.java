/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tecsup.ejb.bean;

import com.tecsup.ejb.dao.ProgramaDAO;
import com.tecsup.ejb.model.Programa;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author alumno
 */
@ManagedBean
@RequestScoped
public class ProgramaMultiBean {

    @Inject
    ProgramaDAO programaDAO;
    private List<Programa> programas = new ArrayList();
    private Programa programa = new Programa();

    public String update(Programa pro) {
        this.programa = pro;        
        return "programaForm.xhtml";
    }

    public String save() {
        
        String redirect = null ; 
        
        if (programa.getId() == null) {
            programaDAO.save(programa);
            
        } else {
            programaDAO.update(programa);
            
        }
        this.programa = new Programa();
        
        return "ProgramaIndex.xhtml";
    }

    public void delete(Programa programa) {
        programaDAO.delete(programa);
    }

    public ProgramaDAO getProgramaDAO() {
        return programaDAO;
    }

    public void setProgramaDAO(ProgramaDAO programaDAO) {
        this.programaDAO = programaDAO;
    }

    public List<Programa> getProgramas() {
        programas = programaDAO.all();
        return programas;
    }

    public void setProgramas(List<Programa> programas) {
        this.programas = programas;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }
    
   
    
}
